# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Add Azure VM wrappers.
- Add input variable to customise the OS disk size

### Changed

- Default OS disk type is now "Standard_LRS"

### Removed
